/*
 * Hercules test inmate
 *
 * University of Modena and Reggio Emilia
 *
 * Authors:
 *  Luca Miccio <lucmiccio@gmail.com>
 *
 * N.B: this inmate should be used for testing only
 */

#include <inmate.h>

#define TEST_PREM 1
#define TEST_MEMGUARD 2

#define TEST_TYPE TEST_MEMGUARD
#define DEBUG

#if (TEST_TYPE == TEST_MEMGUARD)
#define ARRAY_SIZE ((1*1024)/sizeof(int))
#endif

/* This code MUST be equal to the one set in jailhouse/hypercall.h */
#define JAILHOUSE_HC_PREM_CODE 9
#define JAILHOUSE_HC_MEMGUARD_CODE 10

/* Maxmimum number of (lock/unlock) iterations */
#define LOOP_NUM 10

enum prem_phase {
        PREM_COMPATIBLE = 0,
        PREM_MEMORY     = 1,
        PREM_COMPUTE    = 2,
};

/* Memguard flags */
#define MGF_PERIODIC      (1 << 0) /* Chooses between periodic or one-shot budget replenishment */
#define MGF_MASK_INT      (1 << 1) /* Mask (disable) low priority interrupts until next memguard call */

static inline __u64 prem_set(enum prem_phase phase, unsigned long mem_budget, unsigned long timeout,
            unsigned long period, unsigned long flags)
{
        return jailhouse_call_arg5(JAILHOUSE_HC_PREM_CODE, phase, mem_budget, timeout, period, flags);
}

static inline __u64 memguard_call(unsigned long budget_time, unsigned long budget_memory,
 		   unsigned long flags)
{
        return jailhouse_call_arg3(JAILHOUSE_HC_MEMGUARD, budget_time, budget_memory, flags);
}

#if (TEST_TYPE == TEST_PREM)
static char* PHASE(enum prem_phase phase)
{
        switch (phase) {
                case PREM_COMPATIBLE:
                        return "PREM_COMPATIBLE";
                        break;
                case PREM_MEMORY:
                        return "PREM_MEMORY";
                        break;
                case PREM_COMPUTE:
                        return "PREM_COMPUTE";
                        break;
                default:
                        return "ERROR";
                        break;
        }

        return "ERR";
}


static void test_phase_latency(enum prem_phase phase, unsigned long budget, unsigned long timeout,
                    unsigned long period, unsigned long flags)
{
        volatile u64 prem_lat_s, prem_lat_e = 0;
        volatile u64 ret = -1;
        printk("\n### Testing %s phase ###\n", PHASE(phase));
        prem_lat_s = timer_get_ticks();
        ret = prem_set(phase, budget, timeout, period, flags);
        prem_lat_e = timer_get_ticks();

        printk("Prem params:\n");
        printk("Phase: %s\nBudget: %lu\nTimeout: %lu\n", PHASE(phase), budget, timeout);
        printk("HC exited with code: %llu\n",ret);
        printk("HC Latency: %6ld ns\n\n", (long)timer_ticks_to_ns(prem_lat_e - prem_lat_s));
        //printk("Value in ticks Start: %llu End: %llu\n",prem_lat_s,prem_lat_e);

}

#endif

#define ARM64_READ_SYSREG(reg) \
({ \
    u64 _val; \
    __asm__ volatile("mrs %0," #reg : "=r" (_val)); \
    _val; \
})

/* Sleep interface */
#define COUNTS_PER_USECOND 32
static void sleep_common(u32 n, u32 count)
{
        u64 tCur,tEnd;
        tCur = ARM64_READ_SYSREG(CNTPCT_EL0);
        tEnd = tCur + (((u64)n) * count);
        do {
                tCur = ARM64_READ_SYSREG(CNTPCT_EL0);
        } while (tCur < tEnd);
}
int usleep(unsigned long useconds);
int sleep(unsigned long seconds);
int usleep(unsigned long useconds)
{
        sleep_common((u32)useconds, COUNTS_PER_USECOND);
        return 0;
}
int sleep(unsigned long seconds)
{
        usleep(seconds*1000000);
        return 0;
}


void inmate_main(void)
{
        printk("#### Hercules PREM test ####\n");
        //int MY_ID = cmdline_parse_int("ID",-1);
#if(TEST_TYPE == TEST_PREM)
        unsigned long budget = 1;
        unsigned long timeout = 1;

        printk("#### BASIC TESTING #####\n");
        test_phase_latency(PREM_MEMORY,budget,timeout,10,1);
        test_phase_latency(PREM_COMPATIBLE,budget,timeout,10,1);
        test_phase_latency(PREM_COMPUTE,budget,timeout,10,1);

        /* Force memguard reset */
        memguard_call(0,0,0);

        printk("#### SAME PHASE(MEMORY) TESTING #####\n");
        test_phase_latency(PREM_MEMORY,budget,timeout,10,1);
        test_phase_latency(PREM_MEMORY,budget,timeout,1100,1);
        test_phase_latency(PREM_COMPUTE,budget,timeout,10,1);

        /* Force memguard reset */
        memguard_call(0,0,0);

#else /* (TEST_TYPE == TEST_MEMGUARD) */
        int src[ARRAY_SIZE];
        int dst[ARRAY_SIZE];

        u64 delta;

        /* Warm up */
        memset(&dst,0, ARRAY_SIZE);
        memcpy(&dst,&src, ARRAY_SIZE);

	while (1) {
		int i;
		//printk("#### MEMGUARD TEST ####\n");
		//printk("### CALL Budget: 1000 Timeout: 1000 ###\n");
		memguard_call(1000,1000,1);
		delta = timer_get_ticks();
		for (i = 0; i < 1000; i++)
			memcpy(&dst,&src, ARRAY_SIZE);
		delta = timer_get_ticks() - delta;
		//printk("Time: %6ld ns\n\n", (long)timer_ticks_to_ns(delta));

		memset(&dst,0, ARRAY_SIZE);

		//printk("### CALL Budget: 1 Timeout: 1000000 ###\n");
		memguard_call(1000000,1,1);
		delta = timer_get_ticks();
		for (i = 0; i < 1000; i++)
			memcpy(&dst,&src, ARRAY_SIZE);
		delta = timer_get_ticks() - delta;
		//printk("Time: %6ld ns\n\n", (long)timer_ticks_to_ns(delta));

		memset(&dst,0, ARRAY_SIZE);

		//printk("### CALL Buget: 10 Timeout: 10000 ###\n");
		memguard_call(10000,10,1);
		delta = timer_get_ticks();
		for (i = 0; i < 1000; i++)
			memcpy(&dst,&src, ARRAY_SIZE);
		delta = timer_get_ticks() - delta;
		//printk("Time: %6ld ns\n\n", (long)timer_ticks_to_ns(delta));
	}

#endif
        halt();
}
